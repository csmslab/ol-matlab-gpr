%--------------------------------------------------------------------------   
%   Author       : Aldair E. Gongora
%   Lab          : Brown Research Group
%   Institution  : Boston University
%   Date Created : March 22, 2018          
%   Modified     :
%   Name:           Date:        Description:
%--------------------------------------------------------------------------    
%   Description  : 
%   Usage        : 
%   Input: 
%   Output: 
%--------------------------------------------------------------------------
function [ retval ] = gaussLikelihood(xobs,noise,params)

retval = gaussKernel(xobs,xobs,params) + (noise.^2).*eye(length(xobs));

end

