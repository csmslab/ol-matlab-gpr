%--------------------------------------------------------------------------   
%   Author       : Aldair E. Gongora
%   Lab          : Brown Research Group
%   Institution  : Boston University
%   Date Created : March 22, 2018          
%   Modified     :
%   Name:           Date:        Description:
%--------------------------------------------------------------------------    
%   Description  : 
%   Usage        : 
%   Input: 
%   Output: 
%--------------------------------------------------------------------------
function [ypred,ysigma] = updateBayes(xpred,xobs,yobs,noise,params)

predMean = zeroMeanFunc(xpred);
%print(predMean)
obsMean = zeroMeanFunc(xobs);
%print(obsMean)

sigmaPredObs = gaussKernel(xpred,xobs,params)
sigmaObs2 = gaussLikelihood(xobs,noise,params)

%ypred =1; 
 
%a = size(sigmaPredObs) 
%b = size(sigmaObs2)
%c = size(inv(sigmaObs2))
%dd = yobs 
%e =  (sigmaPredObs/sigmaObs2)*(yobs-obsMean)
%f = predMean
%g = obsMean 

ypred = predMean + (sigmaPredObs/sigmaObs2)*(yobs-obsMean)

%a = size(diag(gaussKernel(xpred,xpred,params)))
%b = (sigmaPredObs/sigmaObs2)
%c = sigmaPredObs
%d = size(b)
%e = size(c)


%ysigma = 1;
ysigma = (gaussKernel(xpred,xpred,params)) - ((sigmaPredObs/sigmaObs2)*sigmaPredObs')
%ysigma = diag(ysigma);
end 