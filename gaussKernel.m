%--------------------------------------------------------------------------   
%   Author       : Aldair E. Gongora
%   Lab          : Brown Research Group
%   Institution  : Boston University
%   Date Created : March 22, 2018          
%   Modified     :
%   Name:           Date:        Description:
%--------------------------------------------------------------------------    
%   Description  : 
%   Usage        : 
%   Input: 
%   Output: 
%--------------------------------------------------------------------------
function [sigma] = gaussKernel(x1,x2,params)

if length(x1) == length(x2) 
    x2Mat = repmat(x2',length(x1),1)
    x1Mat = repmat(x1,1,length(x2))
    diff = x1Mat-x2Mat 
    
    sigma = params(1).*exp(-params(2:end)*(diff.^2));
    % params(2:end) 
    %sigma = diag(sigma); 
else 
    xobsMat = repmat(x2',length(x1),1)
    xpredMat = repmat(x1,1,length(x2)) 
    diff = xpredMat - xobsMat 

    sigma = params(1).*exp(-params(2).*(diff.^2)) 

end

