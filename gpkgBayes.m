%--------------------------------------------------------------------------   
%   Author       : Aldair E. Gongora
%   Lab          : Brown Research Group
%   Institution  : Boston University
%   Date Created : March 22, 2018          
%   Modified     :
%   Name:           Date:        Description:
%--------------------------------------------------------------------------    
%   Description  : The following code performs sequential Bayesian
%   optimization for unknown expensive blackbox functions using Knowledge
%   Gradient as the decision policy. The Bayesian optimization technique is
%   built on Gaussian process regression allowing the prediction of a new
%   alternative based on previously observed data. The likelihood function,
%   mean function, and covariance kernel are defined in the body of the
%   code. 
%   Usage        : gpkgBayes(xmin,xmax,dx,expmax,params,noise,xobs,yobs)
%   Input: 
%   xmin   - Vector array of minimum input domain values
%   xmax   - Vector array of maximum input domain values 
%   dx     - Vector array of step size in input domain
%   expmax - Scalar value indicating experimental budget
%   params - Vector array of Gaussian kernel weights
%   noise  - Vector array of experimental noise 
%   xobs   - Vector array of observation inputs
%   yobs   - Vector array of observation outputs
%   Output: 
%   belief
%   KG value
%   recommended experiment 
%   Y/N prompt 
%--------------------------------------------------------------------------
function [xpred,ypred] = gpkgBayes(xmin,xmax,dx,expmax,params,noise,xobs,yobs)

% preallocate arrays 

% xref
n = length(xmin);
m = floor(abs((xmax-xmin))./dx); 
mmax = max(m);
xpred = zeros(mmax,n);
for num = 1:n 
    xref_temp = xmin(num):dx(num):xmax(num);
    xpred(1:length(xref_temp),num) = xref_temp';
end

% initialize number of observations
xobslen = size(xobs,1);
yobslen = length(yobs); 

% iterate until exhaustion of experimental budget

for i =1:expmax 
    
    %% decision policy
    
    xobs(i+xobslen,:) = rand(1,length(mmax)).*(xmax-xmin)+xmin;
    disp('Input selected by policy:');
    disp(xobs(i+xobslen,:)); 
    disp('Randomly generated set of experiment inputs:');
    disp(xobs); 

    %% conduct experiment 
    
    yobs(i+yobslen,1) = truthSim(xobs(i+xobslen,:)) + randn()*(noise).^2; 
    disp('Experiment result:'); 
    disp(yobs(i+yobslen)); 
    disp('Collected experiment results:');
    disp(yobs);

    %% prediction
    
    [ypred,ysigma] = updateBayes(xpred,xobs,yobs,noise,params);
    
    
    
    
    %% plots
   ypredsigma = diag(ysigma); 
   subplot(ceil(sqrt(expmax)),ceil(sqrt(expmax)),i)
   plot(xpred,zeroMeanFunc(xpred),'Color',[0.5843    0.8157    0.9882],'LineWidth',4)
   hold on
   plot(xpred,truthSim(xpred),'Color',[0, 0.5, 0],'LineWidth',4)
   plot(xpred,ypred,'b','LineWidth',4)
   plot(xobs,yobs,'r.','MarkerSize', 20); 
   
   upperbound = ypred+sqrt(ypredsigma);
   lowerbound = ypred-sqrt(ypredsigma);
   plot(xpred,upperbound,'Color',[0.5843    0.8157    0.9882],'LineWidth',2)
   plot(xpred,lowerbound,'Color',[0.5843    0.8157    0.9882],'LineWidth',2)
   
   
   %hold on
   %h = fill([xpred flip(xpred)], [upperbound flip(lowerbound)],[0.5843    0.8157    0.9882]);
   %h = fill([xpred xpred(end:-1:1)], [upperbound flip(lowerbound)],[0.5843    0.8157    0.9882]);
   %opacity =0.6 ; 
   %set(h, 'FaceAlpha', opacity);
   %set(h, 'EdgeColor', 'none');
   
   %axis([xmin xmax -1 1])
   %axis([xmin xmax -2 2])
   title([num2str(i),' experiment(s)'],'FontSize',24)

end 

end

