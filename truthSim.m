%--------------------------------------------------------------------------   
%   Author       : Aldair E. Gongora
%   Lab          : Brown Research Group
%   Institution  : Boston University
%   Date Created : March 22, 2018          
%   Modified     :
%   Name:           Date:        Description:
%--------------------------------------------------------------------------    
%   Description  : 
%   Usage        : 
%   Input: 
%   Output: 
%--------------------------------------------------------------------------
function [yobs] = truthSim(xobs)
% 1D Function
yobs = xobs.*sin(10.*xobs); 

% 2D Function 
%yobs = xobs(1).*exp(-xobs(1).^2 - xobs(2).^2); 


end

