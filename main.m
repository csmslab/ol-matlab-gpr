%--------------------------------------------------------------------------   
%   Author       : Aldair E. Gongora
%   Lab          : Brown Research Group
%   Institution  : Boston University
%   Date Created : March 22, 2018          
%   Modified     :
%   Name:           Date:        Description:
%--------------------------------------------------------------------------    
%   Description  : The following code performs sequential Bayesian
%   optimization for unknown expensive blackbox functions using Knowledge
%   Gradient as the decision policy. The Bayesian optimization technique is
%   built on Gaussian process regression allowing the prediction of a new
%   alternative based on previously observed data. The likelihood function,
%   mean function, and covariance kernel are defined in the body of the
%   code. 
%   Usage        : 
%   Input: 
%   xmin   - Vector array of minimum input domain values
%   xmax   - Vector array of maximum input domain values 
%   dx     - Vector array of step size in input domain
%   expmax - Scalar value indicating experimental budget
%   params - Vector array of Gaussian kernel weights
%   noise  - Vector array of experimental noise 
%   xobs   - Vector array of observation inputs
%   yobs   - Vector array of observation outputs
%--------------------------------------------------------------------------
clear
close all
clc
%% Solver Input 

% input domain

% 1D case
xmin = [0, 20]; 
xmax = [1, 100]; 
dx   = [0.01, 1]; 

% 2D case
%xmin = [0, 0]; 
%xmax = [1, 2]; 
%dx   = [0.01, 0.02]; 

% experimental budget 

expmax = 8; 

% kernel weights 

params = [1 5 6]; 

% experimental noise 

noise = [0.01]; 

% optional: prior observations

xobs = []'; 

yobs = []'; 

% sequential bayesian optimization
[xpred,ypred] = gpkgBayes(xmin,xmax,dx,expmax,params,noise,xobs,yobs); 
