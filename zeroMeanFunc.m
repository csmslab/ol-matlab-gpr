%--------------------------------------------------------------------------   
%   Author       : Aldair E. Gongora
%   Lab          : Brown Research Group
%   Institution  : Boston University
%   Date Created : March 22, 2018          
%   Modified     :
%   Name:           Date:        Description:
%--------------------------------------------------------------------------    
%   Description  : 
%   Usage        : 
%   Input: 
%   Output: 
%--------------------------------------------------------------------------
function [retval] = zeroMeanFunc(x)
retval = zeros(length(x),1); 

end

